# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com
import plotly.graph_objs as go
import numpy as np
import plotly.figure_factory as ff

import xpresso.ai.core.data.visualization.utils as utils
from xpresso.ai.core.data.visualization.plotly.res.plot_types import diagram


# End Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------

class NewPlot(diagram):
    def __init__(self, input_1, input_2, input_3, plot_title=None,
                 output_format=utils.HTML, output_path=None, auto_render=False, filename=None):

        # Apply Def Style to Plot
        self.style_plot()

        if plot_title is not None or filename is not None:
            self.set_labels_manual(plot_label=plot_title, filename=filename)

        # Convert Inputs to Lists
        input_1 = (np.array(input_1).tolist())
        input_2 = (np.array(input_2).tolist())
        input_3 = (np.array(input_3).tolist())

        # Check Input Types
        if (type(input_1) is list and type(input_2) is list and type(input_3) is list):
            # Determine Element Count
            z_axis_elems = 0

            for set in input_3:
                if (not (type(set) is list)):
                    # Return Error on Single Array Dimension
                    print("ERROR: Single Dimensional Array Detected!")
                    utils.halt()
                if (len(set) != len(input_1)):
                    # Swap Lists and Test Alignment
                    temp_input = input_1
                    input_1 = input_2
                    input_2 = temp_input

                    if (len(set) != len(input_1)):
                        # Return Error on Array Alignment
                        print("ERROR: Array dimensions do not align!")
                        utils.halt()
                    else:
                        print("CAUTION: Detected Array Misalignment. Swapped X and Y values!")

                z_axis_elems += len(set)

            # Error Check Array Lengths
            if ((len(input_1) * len(input_2)) != z_axis_elems):
                print("ERROR: Array dimensions do not align!")
                utils.halt()
            else:
                # Set List Values
                self.x_axis_values = input_1
                self.y_axis_values = input_2
                self.z_axis_values = input_3

            # Process Set Arguments
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True,
                            output_path=output_path)
        else:
            # Return Error on Invalid Input
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def style_plot(self):
        # Apply Default Style to Plot
        self.apply_default_style()

        # Apply Custom Style to Plot
        self.marker = None

    def process(self, output_format):

        if output_format is utils.PNG or output_format is utils.PDF:
            # Generate Plot Config for interactive heatmap
            plot_data = ff.create_annotated_heatmap(x=self.x_axis_values,
                                                    y=self.y_axis_values,
                                                    z=self.z_axis_values, hovertemplate=
                                                    self.hover_tool, name=self.graph_name)
        else:
            # Generate Plot Config for interactive heatmap
            plot_data = go.Heatmap(x=self.x_axis_values,
                                   y=self.y_axis_values,
                                   z=self.z_axis_values,
                                   hovertemplate=self.hover_tool, name=self.graph_name)

        # Generate Plot Layout
        plot_layout = go.Layout(autosize=True, width=self.plot_width,
                                height=self.plot_height,
                                paper_bgcolor=str(self.bg_color),
                                plot_bgcolor=str(self.plot_color),
                                title=self.plot_title, xaxis=
                                self.x_axis_title, yaxis=self.y_axis_title,
                                margin=go.layout.Margin(l=int(self.left),
                                                        r=int(self.right),
                                                        b=int(self.bottom),
                                                        t=int(self.top),
                                                        pad=int(self.padding)))

        return {'data': [plot_data], 'layout': plot_layout}

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_single_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------

# End Graph Class---------------------------------------------------------------------------------------------------------------------------------------------------------------
